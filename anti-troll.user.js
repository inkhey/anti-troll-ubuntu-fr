// ==UserScript==
// @name        Anti Trolls
// @namespace   Ubuntu-fr
// @description Filtre les messages des trolls sur ubuntu-fr
// @version     1.1
// @grant       none
// ==/UserScript==
// @include         https://forum.ubuntu-fr.org/*
// @include         https://forum.kubuntu-fr.org/*
// @include         https://forum.xubuntu-fr.org/*
// @grant	    none

/***********************************************************
 * Message de remplacement pour les citations
 ***********************************************************/
var citation_message =  '<img src=https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Internet_Troll.png/120px-Internet_Troll.png>'

/***********************************************************
 * Liste des trolls à filtrer                              *
 * Exemple: TROLL_LIST = ['pseudo1', 'pseudo2', 'pseudo3'] *
 ***********************************************************/
var TROLL_LIST = [];

/**********************************************************************
 * Prédicat pour savoir si l'auteur d'un message est un troll         *
 * author: pseudo de l'auteur                                         *
 * quoted: booléen pour savoir s'il faut vérifier une citation ou non *
 **********************************************************************/
function is_troll(author, quoted)
{
    //suffix = (quoted)?' a écrit':'';
    suffix = (quoted)?' a écrit&nbsp;:':'';

    for(k=0;k<TROLL_LIST.length;k++)
    {
       
        if(author == TROLL_LIST[k] + suffix)
        {
            return true;
        }
    }
    return false

}

/********************************************************************
 * Fonction pour connaître l'auteur d'un message ou d'une citation  *
 * element: arbre représentant le message ou la citation            *
 * tag: tag de l'arbre qui contient le nom de l'auteur              *
 * index: index dans l'arbre du tag qui contient le nom de l'auteur *
 ********************************************************************/
function get_author(element, tag, index)
{
    var tag_list = element.getElementsByTagName(tag);
    if (tag_list.length)
    {
        return tag_list[index].innerHTML;
    }
}


/*****************************************************************
 * Action appliquée à l'élément contenant le message indésirable *
 * Efface le message.                                            *
 * node: noeud de l'arbre correspondant au message               *
 *****************************************************************/
function filter_message(node)
{
    node.style.display = "none";
}

// Remplace le contenu d'une citation en cas de troll
function filter_quote(quote)
{
    author = get_author(quote, 'cite', 0);
    if(is_troll(author, true))
    {
       par = quote.querySelectorAll("blockquote");
       par[0].innerHTML = citation_message;
        
    }
}

// Analyse les citations d'un message et filtre si nécessaire
function check_quotes(mess)
{
    quotes_list = mess.getElementsByClassName('quotebox');
    if(quotes_list.length > 0)
    {
        for(l=0;l<quotes_list.length;l++)
        {
            filter_quote(quotes_list[l])

        }
    }
}

/********************************************************************
 * Analyse les messages et applique les filtres nécessaires         *
 * class_name: nom de la classe identifiant les messages            *
 * tag: tag de l'arbre du message contenant le nom de l'auteur      *
 * index: index dans l'arbre du tag qui contient le nom de l'auteur *
 ********************************************************************/
function check_messages(class_name, tag, index)
{
    mess_list = document.getElementsByClassName(class_name);
    for(i=0;i<mess_list.length;i++)
    {
        // Message à analyser
        current_mess = mess_list[i];
        // Nom de l'auteur du message
        author = get_author(current_mess, tag, index);
        // Filtre le message si l'auteur est un troll
        if(is_troll(author, false))
        {
            filter_message(current_mess);
        }
        // Vérifie les citations du message
        check_quotes(current_mess);

    }
}

/*********************************************************************
 * Analyse la page pour y supprimer toutes les occurences des trolls *
 * type: type de la page.                                            *
 *********************************************************************/
function check_pages(type)
{
    // Page de lecture d'un topic
    if (type == 'viewtopic')
    {
        check_messages('blockpost roweven', 'a', 1);
        check_messages('blockpost rowodd', 'a', 1);
    }
    // Page pour rédiger un message
    else if (type == 'post')
    {
        check_messages('box roweven', 'strong', 0);
        check_messages('box rowodd', 'strong', 0);
    }
}

/****************************************************************
 *                              MAIN                            *
 * analyse l'adresse de la page puis lance la fonction adéquate *
 ****************************************************************/
var path = document.location.pathname;
if (path == '/viewtopic.php')
{

    check_pages('viewtopic');
}
else if (path == '/post.php')
{
    check_pages('post');
}
