# Script greasemonkey Anti-troll pour le forum ubuntu-fr.

## Historique

Le script anti-troll est une antiquité du forum ubuntu-fr qui à
passé les âges. On en retrouve ainsi des traces datant de 2009 et
il n’est exclus que le script soit lui-même encore plus ancien.

Les historiens s’accordent que certains trolls célèbres ont poussés
certains utilisateurs fatigués de tomber encore et encore une fois dans
le panneaux, répondant sérieusement, point par point à des propos
au mieux absurde au pire insultant provenant toujours des même utilisateurs
à la mauvaise foi légendaire, à agir.

Ainsi on ne sait trop par qui et comment ce script à été crée.
Une chose est certaine, le script à évolué quelque-peu durant les années
et reste aujourd’hui utilisé par une petite communauté.

## But
Ce script à pour but purement et simplement de ne pas afficher les commentaires
de certains trolls déterminée comme tel par l’utilisateur avertis afin
que celui-ci puissent vivre sa vie plus tranquillement.

## Configuration :

Il est possible de configurer à la fois la liste des trolls :

```javascript
var TROLL_LIST = ['pseudo1','pseudo2'] ;
```
Mais aussi le message à afficher quand une personne cite un troll (il
convient pour une meilleure lisibilité du topic de savoir que l’utilisateur
répond à troll).

ainsi :

```javascript
var citation_message = '<img src=https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Internet_Troll.png/120px-Internet_Troll.png>'
```

permet d’afficher une superbe icône de troll sympathique en lieu et place d’un
texte assurément de mauvaise foi.

![Troll sympathique](https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Internet_Troll.png/120px-Internet_Troll.png)

## Licence

La question de la licence est problématique du fait même que les auteurs
originaux ne sont pas clairement établis, je les invites à se manifester.
Il semblerais que la licence original soit la GPL v2.
voir [ici](https://forum.ubuntu-fr.org/viewtopic.php?pid=2700750)
et [ici](https://forum.ubuntu-fr.org/viewtopic.php?pid=2751860)

Dans tout les cas du fait même de la forme du script, de son aspect peu original
et de la façon informelle dont il s’est diffusé, il est évident que
le partage et la modification d’un tel script sont autorisés.
